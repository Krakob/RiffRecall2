defmodule RiffRecallTest do
  use ExUnit.Case
  doctest RiffRecall

  @covered_oses [:linux, :nt, :darwin]
  test "os is covered" do
    assert RiffRecall.current_os() in @covered_oses
  end
end
