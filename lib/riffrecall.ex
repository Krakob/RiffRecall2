defmodule RiffRecall do
  @moduledoc """
  Documentation for RiffRecall.
  """

  @doc """

  """

  def current_os do
    elem(:os.type(), 1)
  end

  def default_config_path do
    case current_os() do
      :nt     -> System.user_home() <> "AppData/Local/RiffRecall"
      :linux  -> System.user_home() <> "/.config/RiffRecall"
      :darwin -> System.user_home() <> "/.config/RiffRecall"
    end
  end

  defp determine_config_path([]), do: default_config_path()
  defp determine_config_path([path]), do: path
  # Takes argv, gives us the config path we'll use.

  def main(argv) do
    config_path = determine_config_path argv
    case File.mkdir_p(config_path) do
      {:error, :eacces} -> report(:error, "Tried to create directory #{config_path}, but the program does not have permission!")
      {:error, :enospc} -> report(:error, "Tried to create directory #{config_path}, but the drive doesn't have enough space!")
      {:error, :enotdir} -> report(:error, "Tried to create directory #{config_path}, but one component of the path is a file!")
      :ok -> log(:info, "Directory #{config_path} was created (or already existed)!")
    end
  end
end

