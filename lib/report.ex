defmodule RiffRecall.Report do
  @moduledoc """
  Reporting assistant for RiffRecall.
  Defines a report as a struct:
    * message: string
    * level: one of :info, :debug, :warning, :error); default: :info
    * time: in the format {{year, month, day}, {hour, minute, second}}; default: current time
  Reports should be made with the make function to handle defaulting the timestamp to current time.

  Functions are provided for broadcasting reports to console, tray, and web.
  """

  @enforce_keys [:message, :level, :time]
  defstruct [:message, :level, :time]

  @doc """
  Helper function for making the module's struct with time
  defaulting to a dynamic value. Defaulting it in the defstruct will
  give every struct the default time of compile time, not current time
  """
  def make(message, level \\ :info, time \\ nil) do
    time = time || :calendar.local_time()  # If time was not specified, use current time
    %RiffRecall.Report{message: message, level: level, time: time}
  end

  @colours %{
    debug: IO.ANSI.cyan(),
    info: IO.ANSI.default_color(),
    warning: IO.ANSI.yellow(),
    error: IO.ANSI.red()
  }

  @doc """
  Send report to console, tray, and web client asynchronously.
  """
  def report_all(report) do
    spawn fn -> report_console(report) end
    spawn fn -> report_tray(report) end
    spawn fn -> report_web(report) end
  end

  @doc """
  Send report to console in a pretty format.
  """
  def report_console(report) do
    col_task   = Task.async(fn -> get_colour(report) end)
    time_task  = Task.async(fn -> build_timestamp(report) end)
    level_task = Task.async(fn -> build_level_string(report) end)

    colour       = Task.await col_task
    timestamp    = Task.await time_task
    level_string = Task.await level_task

    IO.puts "#{colour}[#{timestamp} | #{level_string}] #{report.message}"
  end

  @doc """
  Sends a report to system tray. Only warnings and errors go through.
  """
  def report_tray(report) do
    # TODO: display :warning or :error in tray notification
  end

  @doc """
  Sends a report to the web server.
  """
  def report_web(report) do
    # TODO: send report to web server -- it'll know what to do
  end

  @doc """
  Takes a report and formats its timestamp in a string as "hh:mm:ss"
  """
  defp build_timestamp(report) do
    report.time
    |> elem(1)  # Grabs tuple of h, m, s, we discard date.
    |> Tuple.to_list()
    |> Enum.map(fn (x) ->  # Turn int tuple into zero-padded string tuple
      x
      |> Integer.to_string()
      |> String.pad_leading(2, "0")
    end)
    |> Enum.join(":")
  end

  @doc """
  Transforms a report level atom into a prettier string.
  """
  defp build_level_string(report) do
    report.level
    |> Atom.to_string
    |> String.capitalize
  end

  @doc """
  Takes a report and returns the corresponding ASCII-colour code from @colours.
  """
  defp get_colour(report) do
    {:ok, colour} = Map.fetch(@colours, report.level)
    colour
  end
end
