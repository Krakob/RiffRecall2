defmodule RiffRecall.Config do
  defstruct(
    config_path: RiffRecall.default_config_path(),
    buffer_length: 300
  )
end
